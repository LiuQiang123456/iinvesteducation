//
//  UIViewController+TouchHideKeyboard.h
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/19.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (TouchHideKeyboard)
- (void)setTouchHideKeyboard;
@end
