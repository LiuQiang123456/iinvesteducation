//
//  UIViewController+TouchHideKeyboard.m
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/19.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import "UIViewController+TouchHideKeyboard.h"
@implementation UIViewController (TouchHideKeyboard)
- (void)setTouchHideKeyboard{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchHideKey:)];
    [self.view addGestureRecognizer:tap];
}

- (void)touchHideKey:(UITapGestureRecognizer *)tap{
    [self.view endEditing:YES];
}
@end
