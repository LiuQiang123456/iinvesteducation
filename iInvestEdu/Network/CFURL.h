//
//  CFURL.h
//  CFZNNewBuild
//
//  Created by codeFarmer on 2017/6/30.
//  Copyright © 2017年 codeFamer. All rights reserved.
//

#import <Foundation/Foundation.h>
/***************************** baseUrl ******************************/
NSString *baseUrl();

/***************************** funcUrl ******************************/
NSString *loginUrl();
NSString *regUrl();
NSString *vericodeUrl();
NSString *resetPWDUrl();
