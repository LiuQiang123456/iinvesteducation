//
//  NSString+Encoding.h
//  QWeiboSDK4iOS
//
//  Created on 11-1-12.
//  
//

#import <Foundation/Foundation.h>


@interface NSString (QOAEncodingmy)

- (NSString *)myURLEncodedString;
//- (NSString *)URLEncodedString;
- (NSString *)myURLDecodedString;
//- (NSString *)URLDecodedString;

@end
