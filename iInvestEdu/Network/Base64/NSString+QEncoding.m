//
//  NSString+Encoding.m
//  QWeiboSDK4iOS
//
//  Created on 11-1-12.
//  
//

#import "NSString+QEncoding.h"


@implementation NSString (QOAEncodingmy)

//- (NSString *)URLEncodedString 
//{
//    NSString *result = (NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
//                                                                           (CFStringRef)self,
//                                                                           NULL,
//																		   CFSTR("!*'();:@&=+$,/?%#[]"),
//                                                                           kCFStringEncodingUTF8);
//    [result autorelease];
//	return result;
//}
- (NSString *)myURLEncodedString
{
    NSString *result = (NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                           (CFStringRef)self,
                                                                           NULL,
																		   CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                           kCFStringEncodingUTF8);
    [result autorelease];
	return result;
}
//- (NSString*)URLDecodedString
//{
//	NSString *result = (NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,
//																						   (CFStringRef)self,
//																						   CFSTR(""),
//																						   kCFStringEncodingUTF8);
//    [result autorelease];
//	return result;	
//}
- (NSString *)myURLDecodedString
{
    NSString *result = (NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,
																						   (CFStringRef)self,
																						   CFSTR(""),
																						   kCFStringEncodingUTF8);
    [result autorelease];
	return result;
}
@end
