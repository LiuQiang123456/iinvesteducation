//
//  NetWorkTool.m
//  CFZNNewBuild
//
//  Created by codeFarmer on 2017/6/30.
//  Copyright © 2017年 codeFamer. All rights reserved.
//

#import "NetWorkTool.h"
@implementation NetWorkTool
+ (void)load{
    
}
@end

static AFHTTPSessionManager *singleSessionMgr;
static AFNetworkReachabilityManager *singleRAMgr;
#pragma mark - private request funcs
/***************************** session单例 ******************************/

//sessionManager单例，如果不使用单例后果很严重
AFHTTPSessionManager *sharedMgr(){
    if (!singleSessionMgr) {
        singleSessionMgr = [AFHTTPSessionManager manager];
        singleSessionMgr.completionQueue     = dispatch_get_main_queue();
        AFJSONRequestSerializer *serialier   = [AFJSONRequestSerializer serializer];
        singleSessionMgr.requestSerializer   = serialier;
        singleSessionMgr.requestSerializer.timeoutInterval = 20;
        singleSessionMgr.responseSerializer  = [AFHTTPResponseSerializer serializer];
    }
    return singleSessionMgr;
}

/***************************** post请求 ******************************/
NSURLSessionTask *post(NSString *urlStr, NSDictionary *postData, processReq process){
    NSURLSessionTask *task = [sharedMgr() POST:urlStr parameters:postData progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable response) {
        if ([response isKindOfClass:[NSData class]]) {
            NSString *resultStr = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
            response  = [resultStr dataUsingEncoding:NSUTF8StringEncoding];
            NSError *error;
            response = [NSJSONSerialization JSONObjectWithData:response options:(NSJSONReadingMutableContainers) error:&error];
        }
        process(nil, response);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (error.code != NSURLErrorCancelled) {
            process(error, nil);
        }else{
            NSLog(@"task canceled");
        }
    }];
    [task resume];
    return task;
}

NSURLSessionTask *get(NSString *urlStr, NSDictionary *postData, processReq process){
    NSURLSessionTask *task = [sharedMgr() GET:urlStr parameters:postData progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable response) {
        if ([response isKindOfClass:[NSData class]]) {
            NSString *resultStr = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
            response  = [resultStr dataUsingEncoding:NSUTF8StringEncoding];
            NSError *error;
            response = [NSJSONSerialization JSONObjectWithData:response options:(NSJSONReadingMutableContainers) error:&error];
        }
        process(nil, response);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (error.code != NSURLErrorCancelled) {
            process(error, nil);
        }else{
            NSLog(@"task canceled");
        }
    }];
    [task resume];
    return task;
}

/***************************** form表单提交图片 ******************************/
NSURLSessionTask *formImage(NSString *urlStr, NSString *imgName, UIImage *img, NSDictionary *postData, processReq process, processProgress progress){
    return [sharedMgr() POST:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] parameters:postData constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *data = UIImagePNGRepresentation(img);
        [formData appendPartWithFormData:data name:imgName];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        progress(uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject isKindOfClass:[NSData class]]) {
            responseObject = [NSJSONSerialization JSONObjectWithData:responseObject options:(NSJSONReadingAllowFragments) error:nil];
        }
        process(nil, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (error.code != NSURLErrorCancelled) {
            process(error, nil);
        }else{
            NSLog(@"task canceled");
        }
    }];
}

BOOL requestSucceed(NSDictionary *response){
    if ([response valueForKey:@"code"] && [[response valueForKey:@"code"] integerValue] == 0) {
        return YES;
    }
    return NO;
}

NSString *computeErrorMsg(NSError *error, NSDictionary *response){
    if (error) {
        return error.localizedDescription;
    }
    if ([response valueForKey:@"code"] && [[response valueForKey:@"code"] integerValue] != 0) {
        return [response valueForKey:@"msg"] ? [response valueForKey:@"msg"] : @"数据请求出错，请重新尝试";
    }
    return nil;
}

#pragma mark - public request funcs
//reachability
AFNetworkReachabilityManager *sharedRAMgr(){
    if (!singleRAMgr) {
        singleRAMgr = [AFNetworkReachabilityManager sharedManager];
        [singleRAMgr setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
//            [[NSNotificationCenter defaultCenter] postNotificationName:CFNetWorkChangeNotification object:nil];
        }];
        [singleRAMgr startMonitoring];
    }
    return singleRAMgr;
}


