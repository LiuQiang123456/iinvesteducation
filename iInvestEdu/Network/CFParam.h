//
//  CFParam.h
//  CFZNNewBuild
//
//  Created by codeFarmer on 2017/6/30.
//  Copyright © 2017年 codeFamer. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, VeriCodeType) {
    VeriCodeTypeRegister = 0,
    VeriCodeTypeResetPWD = 1,
};

NSDictionary *loginParam(NSString *account, NSString* pwd);
NSDictionary *veriCodeParam(NSString *mobile, VeriCodeType type);
