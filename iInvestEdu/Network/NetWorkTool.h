//
//  NetWorkTool.h
//  CFZNNewBuild
//
//  Created by codeFarmer on 2017/6/30.
//  Copyright © 2017年 codeFamer. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NetWorkTool:NSObject

@end

//noties
extern NSString *const CFNetWorkChangeNotification;

/***************************** block ******************************/
//处理请求错误和数据
typedef void(^processReq)(NSError *error, NSDictionary *responseData);

//处理上传进度
typedef void(^processProgress)(NSProgress *);

/*****************************publick funcs******************************/
//reachability
AFNetworkReachabilityManager *sharedRAMgr();


NSURLSessionTask *post(NSString *urlStr, NSDictionary *postData, processReq process);
NSURLSessionTask *get(NSString *urlStr, NSDictionary *postData, processReq process);
BOOL requestSucceed(NSDictionary *response);
NSString *computeErrorMsg(NSError *error, NSDictionary *response);
