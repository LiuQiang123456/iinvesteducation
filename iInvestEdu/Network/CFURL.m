//
//  CFURL.m
//  CFZNNewBuild
//
//  Created by codeFarmer on 2017/6/30.
//  Copyright © 2017年 codeFamer. All rights reserved.
//

#import "CFURL.h"
/***************************** baseUrl ******************************/
NSString *baseUrl(){
    return @"http://atj.caidao.hexun.com";
}
/***************************** funcUrl ******************************/
NSString *loginUrl(){
    return [baseUrl() stringByAppendingPathComponent:@"wap/passport/login"];
}

NSString *regUrl(){
    return [baseUrl() stringByAppendingPathComponent:@"wap/passport/register"];
}

NSString *vericodeUrl(){
    return [baseUrl() stringByAppendingPathComponent:@"wap/passport/sendcode"];
}

NSString *resetPWDUrl(){
    return [baseUrl() stringByAppendingPathComponent:@"wap/passport/edit"];
}

NSString *homeDataUrl(){
    return [baseUrl() stringByAppendingPathComponent:@"wap/index/index"];
}

NSString *roomInfoUrl(){
    return [baseUrl() stringByAppendingPathComponent:@"wap/index/roominfo"];
}

NSString *roomProductInfoUrl(){
    return [baseUrl() stringByAppendingPathComponent:@"wap/order/index"];
}
