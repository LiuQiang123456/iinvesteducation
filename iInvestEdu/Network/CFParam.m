//
//  CFParam.m
//  CFZNNewBuild
//
//  Created by codeFarmer on 2017/6/30.
//  Copyright © 2017年 codeFamer. All rights reserved.
//

#import "CFParam.h"

NSDictionary *loginParam(NSString *account, NSString* pwd){
    return @{
             @"username" : account,
             @"password" : pwd
             };
}

NSDictionary *veriCodeParam(NSString *mobile, VeriCodeType type){
    return @{
             @"mobile" : mobile,
             @"type"   : @(type)
             };
}
