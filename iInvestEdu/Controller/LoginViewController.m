//
//  LoginViewController.m
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/23.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgetPwdViewController.h"
#import "RegisterViewController.h"
#import "APPUserManager.h"
@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *accField;
@property (weak, nonatomic) IBOutlet UITextField *pwdField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *regBtn;
@property (weak, nonatomic) IBOutlet UIButton *resetBtn;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTouchHideKeyboard];
    self.navigationItem.leftBarButtonItem   = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:(UIBarButtonItemStylePlain) target:self action:@selector(leftBarBtnAction)];
    self.navigationItem.title   = @"登录";
    
    _loginBtn.layer.cornerRadius  = 2;
    _loginBtn.layer.masksToBounds = YES;
    _accField.leftView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, 20, 16))];
    UIImageView *leftImg0 = [[UIImageView alloc]initWithFrame:_accField.leftView.bounds];
    leftImg0.contentMode  = UIViewContentModeScaleAspectFit;
    leftImg0.image   = [UIImage imageNamed:@"ico_1.jpg"];
    [_accField.leftView addSubview:leftImg0];
    _accField.leftViewMode  = UITextFieldViewModeAlways;
    
    
    _pwdField.leftView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, 20, 16))];
    UIImageView *leftImg = [[UIImageView alloc]initWithFrame:_pwdField.leftView.bounds];
    leftImg.contentMode  = UIViewContentModeScaleAspectFit;
    leftImg.image   = [UIImage imageNamed:@"ico_2.jpg"];
    [_pwdField.leftView addSubview:leftImg];
    _pwdField.leftViewMode  = UITextFieldViewModeAlways;

}
#pragma mark - navi actions
- (void)leftBarBtnAction{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - btn actions

- (IBAction)buttonActions:(UIButton *)sender {
    [self.view endEditing:YES];
    if (sender == _loginBtn) {
        if (_accField.text.length == 0) {
            [self showMessage:@"手机号/用户名不能为空"];
            return;
        }
        if (_pwdField.text.length == 0) {
            [self showMessage:@"密码不能为空"];
            return;
        }
        [self showWaiting];
        get(loginUrl(), loginParam(_accField.text, _pwdField.text), ^(NSError *error, NSDictionary *response) {
            [self hideAllHUD];
            if (!computeErrorMsg(error, response)) {
                NSDictionary *result = [response valueForKey:@"data"];
                [APPUserManager sharedManager].user = [UserModel mj_objectWithKeyValues:result];
            }else{
                [self showMessage:computeErrorMsg(error, response)];
            }
        });
    }
    else if (sender == _regBtn) {
        RegisterViewController *reg = [[RegisterViewController alloc] init];
        [self.navigationController pushViewController:reg animated:YES];
    }
    else if (sender == _resetBtn) {
        ForgetPwdViewController *forget = [[ForgetPwdViewController alloc] init];
        [self.navigationController pushViewController:forget animated:YES];
    }
}
@end
