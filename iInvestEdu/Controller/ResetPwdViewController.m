//
//  ResetPwdViewController.m
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/23.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import "ResetPwdViewController.h"

@interface ResetPwdViewController ()

@end

@implementation ResetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title   = @"修改密码";
    [self setTouchHideKeyboard];
}

@end
