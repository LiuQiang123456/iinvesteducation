//
//  WLNaviController.m
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/25.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import "WLNaviController.h"

@interface WLNaviController ()

@end

@implementation WLNaviController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [super pushViewController:viewController animated:animated];
    //iphoneX tabbar navibar在push时的抖动问题
    if (isIphoneX) {
        CGRect frame = self.tabBarController.tabBar.frame;
        frame.origin.y = [UIScreen mainScreen].bounds.size.height - frame.size.height;
        self.tabBarController.tabBar.frame = frame;
        
        frame = self.navigationBar.frame;
        frame.origin.y = SystemStatusBarH;
        self.navigationBar.frame    = frame;
    }
}

@end
