//
//  RegisterViewController.m
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/24.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import "RegisterViewController.h"
#define veriDur 120

#import "CFURL.h"
#import "CFParam.h"
@interface RegisterViewController ()
@property (weak, nonatomic) IBOutlet UITextField *accField;
@property (weak, nonatomic) IBOutlet UITextField *pwdField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *vericodeField;
@property (weak, nonatomic) IBOutlet UIButton *vericodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (weak, nonatomic) IBOutlet UIButton *protocolBtn;
@property (weak, nonatomic) IBOutlet UIButton *regBtn;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSInteger timerDuration;
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTouchHideKeyboard];
    self.navigationItem.title   = @"注册";
    [self wl_setupViews];
    self.timerDuration  = veriDur;
}

- (void)wl_setupViews{
    _accField.leftViewMode = UITextFieldViewModeAlways;
    _accField.leftView = [self leftViewWithImageName:@"ico_1.jpg"];
    
    _pwdField.leftViewMode = UITextFieldViewModeAlways;
    _pwdField.leftView = [self leftViewWithImageName:@"ico_2.jpg"];
    
    _phoneField.leftViewMode = UITextFieldViewModeAlways;
    _phoneField.leftView = [self leftViewWithImageName:@"ico_3.jpg"];
    
    _regBtn.layer.cornerRadius = 2;
    _regBtn.layer.masksToBounds = YES;
    
    [_vericodeBtn setTitle:@"获取验证码" forState:(UIControlStateNormal)];
    _vericodeBtn.layer.borderColor  = [UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:1].CGColor;
    _vericodeBtn.layer.cornerRadius = 2;
    _vericodeBtn.layer.borderWidth  = 1;
    _vericodeBtn.layer.masksToBounds = YES;
    [_vericodeBtn setTitleColor:[UIColor grayColor] forState:(UIControlStateDisabled)];
    [_vericodeBtn setTitleColor:[UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:1] forState:(UIControlStateNormal)];
    
    _agreeBtn.imageEdgeInsets   = UIEdgeInsetsMake(5, 2, 5, 80);
    _agreeBtn.titleEdgeInsets   = UIEdgeInsetsMake(0, -10, 0, 0);
    _protocolBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
}

- (UIView *)leftViewWithImageName:(NSString *)name{
    UIView *leftView2 = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, 20, 16))];
    UIImageView *leftImg2 = [[UIImageView alloc] initWithFrame:leftView2.bounds];
    leftImg2.image = [UIImage imageNamed:name];
    leftImg2.contentMode = UIViewContentModeScaleAspectFit;
    [leftView2 addSubview:leftImg2];
    return leftView2;
}

- (IBAction)veriBtnAction:(UIButton *)sender {
    if (_phoneField.text.length == 0) {
        [self showMessage:@"手机号不能为空"];
        return;
    }
    if (_phoneField.text.length != 11 || ![_phoneField.text hasPrefix:@"1"]) {
        [self showMessage:@"请输入正确的手机号"];
        return;
    }
    
    [self showWaiting];
    sender.enabled  = NO;
    [self.view endEditing:YES];
    get(vericodeUrl(), veriCodeParam(_phoneField.text, VeriCodeTypeRegister), ^(NSError *error, NSDictionary *responseData) {
        [self hideAllHUD];
        if (!computeErrorMsg(error, responseData)){
            if (_timerDuration == veriDur) {
                [self showMessage:@"验证码发送成功，请注意查收"];
                self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
            }
        }else{
            sender.enabled = YES;
            [self showMessage:computeErrorMsg(error, responseData)];
        }
    });
}

- (void)timerAction:(NSTimer *)timer{
    if (_timerDuration >= 0) {
        _timerDuration --;
        //还没加请求  这直接设置NO了
        _vericodeBtn.enabled = NO;
        _vericodeBtn.layer.borderColor  = [UIColor grayColor].CGColor;
        [_vericodeBtn setTitle:[NSString stringWithFormat:@"%ldS", (long)_timerDuration] forState:(UIControlStateDisabled)];
    }else{
        _timerDuration = veriDur;
        _vericodeBtn.enabled = YES;
        _vericodeBtn.layer.borderColor  = [UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:1].CGColor;
        [_timer invalidate];
        self.timer = nil;
    }
}

- (IBAction)agreeBtnAction:(UIButton *)sender {
    [self.view endEditing:YES];
    sender.selected = !sender.selected;
}

- (IBAction)protocolBtnAction:(UIButton *)sender {
    [self.view endEditing:YES];
}

- (IBAction)regBtnAction:(UIButton *)sender {
    [self.view endEditing:YES];
    if (_accField.text.length == 0) {
        [self showMessage:@"昵称不能为空"];
        return;
    }
    if (_pwdField.text.length < pwdLimite || _pwdField.text.length > pwdMax) {
        [self showMessage:@"请输入6-20位密码"];
        return;
    }
    if (_phoneField.text.length != 11 || ![_phoneField.text hasPrefix:@"1"]) {
        [self showMessage:@"请输入正确的手机号"];
        return;
    }
    if (_vericodeField.text.length != veriCodeLen) {
        [self showMessage:@"请输入正确的验证码"];
        return;
    }
    if (_agreeBtn.selected == NO) {
        [self showMessage:@"请阅读并同意我们的用户协议"];
        return;
    }
    [self showWaiting];
    NSDictionary *para = @{
                           @"username" : _accField.text,
                           @"mobile"   : _phoneField.text,
                           @"password" : _pwdField.text,
                           @"code"     : _vericodeField.text
                           };
    get(regUrl(), para, ^(NSError *error, NSDictionary *responseData) {
        [self hideAllHUD];
        NSString *msg = computeErrorMsg(error, responseData);
        if (!msg) {
            NSDictionary *result = [responseData valueForKey:@"data"];
            [APPUserManager sharedManager].user = [UserModel mj_objectWithKeyValues:result];
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }else{
            [self showMessage:msg];
        }
    });
}


@end
