//
//  ForgetPwdViewController.m
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/23.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import "ForgetPwdViewController.h"
#define veriDur 120
@interface ForgetPwdViewController ()
@property (weak, nonatomic) IBOutlet UITextField *accField;
@property (weak, nonatomic) IBOutlet UITextField *verField;
@property (weak, nonatomic) IBOutlet UITextField *pwdField;
@property (weak, nonatomic) IBOutlet UITextField *reaptPwdField;
@property (weak, nonatomic) IBOutlet UIButton *veriBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSInteger timerDuration;
@end

@implementation ForgetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTouchHideKeyboard];
    
    self.navigationItem.title   = @"忘记密码";
    _confirmBtn.layer.cornerRadius = 2;
    _confirmBtn.layer.masksToBounds = YES;
    
    _veriBtn.layer.borderColor = [UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:1].CGColor;
    _veriBtn.layer.borderWidth = 1;
    _veriBtn.layer.cornerRadius = 2;
    _veriBtn.layer.masksToBounds = YES;
    
    [_veriBtn setTitleColor:[UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:1] forState:(UIControlStateNormal)];
    [_veriBtn setTitle:@"获取验证码" forState:(UIControlStateNormal)];
    [_veriBtn setTitleColor:[UIColor grayColor] forState:(UIControlStateDisabled)];
    
    self.timerDuration  = veriDur;
    
    UIView *leftView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, 20, 16))];
    UIImageView *leftImg = [[UIImageView alloc] initWithFrame:leftView.bounds];
    leftImg.contentMode = UIViewContentModeScaleAspectFit;
    leftImg.image   = [UIImage imageNamed:@"ico_3.jpg"];
    [leftView addSubview:leftImg];
    _accField.leftView  = leftView;
    _accField.leftViewMode  = UITextFieldViewModeAlways;
    
    
    UIView *leftView1 = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, 20, 16))];
    UIImageView *leftImg1 = [[UIImageView alloc] initWithFrame:leftView1.bounds];
    leftImg1.contentMode = UIViewContentModeScaleAspectFit;
    leftImg1.image   = [UIImage imageNamed:@"ico_2.jpg"];
    [leftView1 addSubview:leftImg1];
    _pwdField.leftView  = leftView1;
    _pwdField.leftViewMode  = UITextFieldViewModeAlways;
    
    UIView *leftView2 = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, 20, 16))];
    UIImageView *leftImg2 = [[UIImageView alloc] initWithFrame:leftView1.bounds];
    leftImg2.contentMode = UIViewContentModeScaleAspectFit;
    leftImg2.image   = [UIImage imageNamed:@"ico_2.jpg"];
    [leftView2 addSubview:leftImg2];
    _reaptPwdField.leftView  = leftView2;
    _reaptPwdField.leftViewMode  = UITextFieldViewModeAlways;
}

- (IBAction)veriBtnAction:(UIButton *)sender {
    if (_accField.text.length != 11 || ![_accField.text hasPrefix:@"1"]) {
        [self showMessage:@"请输入正确的手机号"];
        return;
    }
    
    [self showWaiting];
    sender.enabled  = NO;
    [self.view endEditing:YES];
    get(vericodeUrl(), veriCodeParam(_accField.text, VeriCodeTypeResetPWD), ^(NSError *error, NSDictionary *responseData) {
        [self hideAllHUD];
        if (!computeErrorMsg(error, responseData)){
            if (_timerDuration == veriDur) {
                [self showMessage:@"验证码发送成功，请注意查收"];
                self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
            }
        }else{
            sender.enabled = YES;
            [self showMessage:computeErrorMsg(error, responseData)];
        }
    });
}

- (void)timerAction:(NSTimer *)timer{
    if (_timerDuration >= 0) {
        _timerDuration --;
        //还没加请求  这直接设置NO了
        _veriBtn.enabled = NO;
        _veriBtn.layer.borderColor  = [UIColor grayColor].CGColor;
        [_veriBtn setTitle:[NSString stringWithFormat:@"%ldS", (long)_timerDuration] forState:(UIControlStateDisabled)];
    }else{
        _timerDuration = veriDur;
        _veriBtn.enabled = YES;
        _veriBtn.layer.borderColor  = [UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:1].CGColor;
        [_timer invalidate];
        self.timer = nil;
    }
}
- (IBAction)confirmBtnAction:(UIButton *)sender {
    [self.view endEditing:YES];
    [self showWaiting];
    get(resetPWDUrl(), @{@"mobile" : _accField.text, @"password" : _pwdField.text, @"code" : _verField.text}, ^(NSError *error, NSDictionary *responseData) {
        [self hideAllHUD];
        NSString *msg = computeErrorMsg(error, responseData);
        if (!msg) {
            [self showMessage:@"密码修改成功"];
            
        }else{
            [self showMessage:msg];
        }
    });
}

@end
