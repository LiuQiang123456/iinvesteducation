//
//  BaseViewController.h
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/18.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "NetWorkTool.h"
#import "CFURL.h"
#import "CFParam.h"
#import "UIViewController+TouchHideKeyboard.h"
@interface BaseViewController : UIViewController
@property (nonatomic, strong) NSMutableSet <MBProgressHUD *>*HUDSet;
@property (nonatomic, strong) NSMutableSet <NSURLSessionTask *>*taskSet;

- (void)showWaiting;
- (void)showMessage:(NSString *)message;
- (void)hideAllHUD;
@end
