//
//  VideoViewController.m
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/25.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import "VideoViewController.h"

@interface VideoViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *collection;
@end

@implementation VideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    CGFloat itemWid = 167;
    if ([UIScreen mainScreen].bounds.size.width == 414){
        itemWid = (167 / 0.8954);
    }else{
        itemWid = 167 * 0.835;
    }
    layout.itemSize = CGSizeMake(itemWid, 100);
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
//    layout.headerReferenceSize = CGSizeMake(0, 137 * ([UIScreen mainScreen].bounds.size.width / 375.0));
    self.collection = [[UICollectionView alloc] initWithFrame:(CGRectZero) collectionViewLayout:layout];
    [self.view addSubview:_collection];
    UIEdgeInsets inset = UIEdgeInsetsMake(kTopMargin, 0, 0, 0);
    [_collection mas_makeConstraints:^(MASConstraintMaker *make) {
        __weak typeof(self) weakSelf = self;
        make.edges.equalTo(weakSelf.view).insets(inset);
    }];
    _collection.delegate    = self;
    _collection.dataSource  = self;
    _collection.backgroundColor = [UIColor blackColor];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

@end
