//
//  BaseViewController.m
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/18.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import "BaseViewController.h"
#define HUDDur 1.2

@interface BaseViewController ()

@end

@implementation BaseViewController
- (void)dealloc{
    if (_HUDSet) {
        [_HUDSet removeAllObjects];
    }
    if (_taskSet) {
        for (NSURLSessionTask *task in _taskSet) {
            [task cancel];
        }
        [_taskSet removeAllObjects];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"%@ dealloced", NSStringFromClass([self class]));
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIBarButtonItem *item = [[UIBarButtonItem alloc] init];
    item.title  = @"";
    self.navigationItem.backBarButtonItem   = item;
}

- (NSMutableSet<NSURLSessionTask *> *)taskSet{
    if (!_taskSet) {
        _taskSet = [[NSMutableSet alloc] init];
    }
    return _taskSet;
}

- (NSMutableSet<MBProgressHUD *> *)HUDSet{
    if (!_HUDSet) {
        _HUDSet = [[NSMutableSet alloc] init];
    }
    return _HUDSet;
}

- (void)showWaiting{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.HUDSet addObject:hud];
}

- (void)showMessage:(NSString *)message{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode    = MBProgressHUDModeText;
    hud.label.text  = message;
    [hud showAnimated:NO];
    [hud hideAnimated:YES afterDelay:HUDDur];
    hud.removeFromSuperViewOnHide   = YES;
    
}

- (void)hideAllHUD{
    for (MBProgressHUD *hud in _HUDSet) {
        if (hud.mode != MBProgressHUDModeIndeterminate) {
            continue;
        }
        hud.removeFromSuperViewOnHide   = YES;
        [hud hideAnimated:YES];
    }
    if (_HUDSet) {
        _HUDSet = nil;
    }
}
@end
