//
//  APPUserManager.m
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/25.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import "APPUserManager.h"
static APPUserManager *shared;
@implementation UserModel
- (id)valueForUndefinedKey:(NSString *)key{
    return nil;
}
- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}
@end

@implementation APPUserManager
#pragma mark - process user
- (void)saveUser{
    if (!self.user) {
        return;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[self.user mj_keyValues] forKey:kSavedUserDefaults];
    [defaults synchronize];
}

- (void)loadUser{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *userInfo = [defaults valueForKey:kSavedUserDefaults];
    if (userInfo) {
        UserModel *user = [UserModel mj_setKeyValues:userInfo];
        if (user) {
            self.user = user;
        }
    }
}

- (void)setUser:(UserModel *)user{
    _user = user;
    [self saveUser];
}

#pragma mark - singleton and initialize
+ (void)load{
    [super load];
    [APPUserManager sharedManager];
}

+ (instancetype)sharedManager{
    shared = [[APPUserManager alloc] init];
    [shared loadUser];
    return shared;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [super allocWithZone:zone];
    });
    return shared;
}

- (instancetype)init{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [super init];
    });
    return shared;
}

- (id)copy{
    return shared;
}

@end

