//
//  APPUserManager.h
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/25.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MJExtension/MJExtension.h>
@class APPUserManager;
@interface UserModel : NSObject
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *mobile;
@end

@interface APPUserManager : NSObject
@property (nonatomic, strong) UserModel *user;
+ (instancetype)sharedManager;
- (void)saveUser;
@end
