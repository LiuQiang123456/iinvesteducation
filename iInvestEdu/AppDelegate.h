//
//  AppDelegate.h
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/18.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

