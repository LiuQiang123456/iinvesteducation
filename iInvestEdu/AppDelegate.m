//
//  AppDelegate.m
//  iInvestEdu
//
//  Created by fantasineer on 2018/1/18.
//  Copyright © 2018年 fantasineer. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "VideoViewController.h"
#import "WLNaviController.h"
#import "LectureViewController.h"
#import "LivingViewController.h"
#import "MineViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self initializeViews];
    return YES;
}


- (void)initializeViews{
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _window.backgroundColor = [UIColor whiteColor];
    [_window makeKeyAndVisible];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:1]];
    
    
    UITabBarController *tab = [[UITabBarController alloc] init];
    
    VideoViewController *video = [[VideoViewController alloc] init];
    WLNaviController *video_navi = [[WLNaviController alloc] initWithRootViewController:video];
    video_navi.tabBarItem = ({
        NSString *title = @"视频";
        UIImage *img = [UIImage imageNamed:@""];
        NSInteger tag = 0;
        [[UITabBarItem alloc] initWithTitle:title image:img tag:tag];
    });
    
    LectureViewController *lec = [[LectureViewController alloc] init];
    WLNaviController *lec_navi = [[WLNaviController alloc] initWithRootViewController:lec];
    lec_navi.tabBarItem = ({
        NSString *title = @"讲师";
        UIImage *img = [UIImage imageNamed:@""];
        NSInteger tag = 1;
        [[UITabBarItem alloc] initWithTitle:title image:img tag:tag];
    });
    
    LivingViewController *living = [LivingViewController new];
    WLNaviController *living_navi = [[WLNaviController alloc] initWithRootViewController:living];
    living_navi.tabBarItem = ({
        NSString *title = @"直播";
        UIImage *img = [UIImage imageNamed:@""];
        NSInteger tag = 2;
        [[UITabBarItem alloc] initWithTitle:title image:img tag:tag];
    });
    
    MineViewController *mine = [[MineViewController alloc] init];
    WLNaviController *mine_navi = [[WLNaviController alloc] initWithRootViewController:mine];
    mine_navi.tabBarItem  = ({
        NSString *title = @"我的";
        UIImage *img = [UIImage imageNamed:@""];
        NSInteger tag = 3;
        [[UITabBarItem alloc] initWithTitle:title image:img tag:tag];
    });
    
    tab.viewControllers = @[video_navi, lec_navi, living_navi, mine_navi];
    _window.rootViewController = tab;
}
@end
